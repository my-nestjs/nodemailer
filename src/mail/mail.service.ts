import { Injectable } from '@nestjs/common';
import { MailerService } from '@nestjs-modules/mailer';

@Injectable()
export class MailService {
  constructor(private readonly mailerService: MailerService) {}

  async sendEmail(text: string, subject: string) {
    try {
      await this.mailerService.sendMail({
        to: 'k.vorrapong.dev@gmail.com',
        // from: '"Support Team" <support@example.com>', // override default from
        subject: subject,
        text: text,
        // template: './confirmation', // `.hbs` extension is appended automatically
      });
    } catch (error) {
      return error;
    }
    return {
      to: `k.vorrapong.dev@gmail.com`,
      subject: subject,
      text: text,
    };
  }

  async sendEmailWithHbs(text: string, subject: string) {
    await this.mailerService.sendMail({
      to: 'k.vorrapong.dev@gmail.com',
      subject: subject,
      template: '../mail/templates/confirmation.hbs', // `.hbs` extension is appended automatically
      context: {
        // ✏️ filling curly brackets with content
        name: 'Mr.IceiCeicE',
        url: 'google.com',
      },
    });
  }

  async sendEmailWithHtml(text: string, subject: string) {
    await this.mailerService.sendMail({
      to: 'k.vorrapong.dev@gmail.com',
      subject: subject,
      template: '../mail/templates/index.ejs', // `.hbs` extension is appended automatically
      // context: {
      //   // ✏️ filling curly brackets with content
      //   ice: 'Mr.IceiCeicE',
      // },
    });
  }
}

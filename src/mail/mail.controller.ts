import { Body, Controller, Get, Post } from '@nestjs/common';
import { MailService } from './mail.service';

@Controller('mail')
export class MailController {
  constructor(private mailService: MailService) {}
  @Post('/test')
  sendMail(@Body() body: any) {
    console.log('/mail/test');
    let res = this.mailService.sendEmail(body.text, body.subject);
    return res;
  }

  @Post('/hbs')
  sendMailwithHbs(@Body() body: any) {
    console.log('/mail/hbs');
    let res = this.mailService.sendEmailWithHbs(body.text, body.subject);
    return res;
  }

  @Post('/html')
  sendMailwithHtml(@Body() body: any) {
    console.log('/mail/html');
    let res = this.mailService.sendEmailWithHtml(body.text, body.subject);
    return res;
  }
}
